# Oniro membership prospects meeting 2022wk02

* Chair: Agustin
* Scriba: Agustin
* Schedule: Friday 2022-01-14 from 10:00 to 10:30 UTC


## Participants

(add/erase participants)

Andrea G. (Linaro), Agustin B. (EF), Gael B. (EF), Chiara DF (Huawei), Carlo P. (Array), Adrian O'S (Huawei), Davide R. (Huawei), Amit K. (Huawei), Patrick O (NOI), Andrea B. (Synesthesia), Gianluca V. (Seco), Francesco P (). 

## Agenda

* Prospects follow up - Agustin 10 min
* Mobile World Congress - Agustin 10 min
* AOB - 10 min

## MoM

### Prospects follow up

Presentation

* Agustin presented the updates
   * Carlo needs to upload the documentation.
   * No news for Angelo

Discussion

   * Conversation about the potential relation between the Linaro Ledge and Oniro
      * #task Andrea and Davide will have a meeting to prepare how to build that relation. 
	* Seco explains his strategy in relation with Oniro and microcontrollers

### Mobile World Congress

Presentation

* Agustin and Gael confirmed
* 8 meeting slots

Discussion

* #task Monday meeting between Chiara and Agustin to prepare the companies we want to talk to at MWC. 

### AOB

* Gael points out that since we are having meetings with organizations already, we need a template for Oniro presentations. We need to iterate on the existing one.
* Gael points out that he has conflicts with the current slot. Can we change it?
   * Agustin points out that, given that the current meeting series will change from Jan 27th, can we wait until then?

## Relevant links

* Repo: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management
* Actions workflow board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880
* Actions priority board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892 
* Other meetings: #link https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings 
