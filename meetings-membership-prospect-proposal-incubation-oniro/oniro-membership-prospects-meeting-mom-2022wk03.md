# Oniro membership prospects meeting 2022wk03

* Chair: Agustin
* Scriba: Agustin
* Schedule: Friday 2022-01-21 from 10:00 to 10:30 UTC


## Participants

(add/erase participants)

Sebastian S(Huawei).Andrea G. (Linaro), Agustin B. (EF), Gael B. (EF), Chiara DF (Huawei), Carlo P. (Array), Adrian O'S (Huawei), Davide R. (Huawei), Amit K. (Huawei). 

## Agenda

* Prospects follow up - Agustin 10 min
* How Huawei is managing the prospects - Seba 10 min
* Mobile World Congress - Gael 5  min
* AOB - 5 min

## MoM

### Prospects follow up

Presentation

Added some organizations to the pipeline
Oniro vs prpl foundation
For big companies, let's request that they provide to us a technical person so we build a quick demo with them to show executives: show capabilities. 

Discussion



### How Huawei is managing the prospects

Presentation

Sebastian shows a 3 step process and how they do it at Huawei

Discussion

* Agustin suggest to simplify it to one single step process
   * #task Meeting between Agustin and Sebastian to agree on the columns that the spreadsheet to track the prospects should have.


### Mobile World Congress

Presentation

* Adrian describe what we will have at MWC at the room for March 1st and 2nd
   * We can have several companies together if we will present the same.
   * Room close to the venue
   * Huawei is participating/organising a variety of aside events related with standards some of them.
   * All is sorted out to show slides and for remote audience to join.

Discussion

#task For EF brings rollers/signage
#task image/branding for the screens
   * Gael will take them.
* Pressure to schedule meetings.
   * EF and Huawei are working on it,
* Room close to the venue well received


### AOB (ni time)



## Relevant links

* Repo: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management
* Actions workflow board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880
* Actions priority board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892 
* Other meetings: #link https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings 
