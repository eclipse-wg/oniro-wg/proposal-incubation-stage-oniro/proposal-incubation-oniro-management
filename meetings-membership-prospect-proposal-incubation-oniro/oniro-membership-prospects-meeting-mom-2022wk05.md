# Oniro membership prospects meeting 2022wk05

* Chair: Agustin
* Scriba: Agustin
* Schedule: Friday 2022-02-04 from 10:00 to 10:30 UTC


## Participants

(add/erase participants)

Andrea G. (Linaro), Agustin B. (EF), Gael B. (EF), Chiara DF (Huawei), Carlo P. (Array), Adrian O'S (Huawei), Gianluca V (Seco). 

## Agenda

* New prospects pipeline management proposal - Agustin 10 min
* Mobile World Congress - Agustin 15  min
* AOB - 5 min

## MoM

### New prospects pipeline management proposal

Presentation

* Agustin showed the new proposal which is the result of the effort done between Sebastian S. and Agustin himself. The proposal has in consideration the following standards:
   * [GDPR](https://gdpr-info.eu/)
   * Eclipse Foundation [By-Laws](https://www.eclipse.org/org/documents/eclipse-foundation-be-bylaws-en.pdf)
   * Eclipse [Antitrust Compliance](https://www.eclipse.org/org/documents/Eclipse_Antitrust_Policy.pdf) Policy 

The current process has been designed having the following goals in mind:
* Collaboration among a restricted group:
   * By default, only the SC Members have access to the pipeline management. Under request by the SC, other representatives can access to it. As an exceptional case, we will allow a handful of additional people from the SC Members to accesses too.
   * Every people with access have similar rights. We will determine a subset of people that can approve MR (one per SC member and several from the EF related with Oniro). 
* Simplicity:
   * We have taken a step further compared to tracking the prospect's pipeline through a simple spreadsheet.
   * Still, we believe this process is fairly simple.
* Extensibility:
   * The current processes can be adapted to meet more complexity.
* Scalability
   * This process and the visualizations scale better than spreadsheets.
* Other
   * With this process, we prevent the need to use Google Sheets, which was a request from Members.
   
The dashboards defined to visualise and manage the information are presented as a way to also structure the prospect meetings.

Next steps
   
* Agustin will send in written form the details of the proposal so it will be evaluated by Members representatives and especially the Oniro WG SC.
* The proposal will be an agenda topic next week at the Oniro prospects pipeline management meeting (Friday).
* If there are no objections by Oniro Members representatives, we will proceed with the proposal.
* During the first Oniro SC meeting, the proposal will be approved or modified. 

Agustin: thank you Sebastian S. for the effort and ideas put in this proposal.

Discussion

* General agreement of the convenience of the dashboards shown. 
   * Risk of those dashboards being less effective if the information to track gets high.
* Chiara points out that the management of leads, before they become prospects, is missing in the current proposal. She will present at the next marketing meeting her ideas about this specific point.
* Who will introduce and keep the information curated?
   * There is a general expectation that the Member representatives and their alternates focus on the business development activities. The additional people appointed by the SSC Members, together with the Oniro Program Manager will take care of keeping the information curated and present the reports to the involved Member reps. 


### Mobile World Congress 2022

Presentation

The decision taken during the marketing meeting this Wednesday February 2nd has been reconsidered. 
* The rooms will not be cancelled
* The recording session will take place.
* Invitations will be sent.

Adrian presented the content of the MWC 2022 wiki page #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Events/MWC>. The page describes the priorities in terms of invitation.

General call for action to contact people potentially interested in meeting/joining us at MWC 2022.

Discussion

* #task Agustin to configure and share the event calendar
* #task Chiara to provide a generic text for the invitations and reminders.

### AOB

* Agustin propose to Oniro Members to schedule 1:1s with him (EF) to, among other topics, to talk about the pipeline and actions to promote a high return of value from their participation at Oniro.

## Relevant links

* Repo: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management>
* Actions workflow board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880>
* Actions priority board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892> 
* Other meetings: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings> 
