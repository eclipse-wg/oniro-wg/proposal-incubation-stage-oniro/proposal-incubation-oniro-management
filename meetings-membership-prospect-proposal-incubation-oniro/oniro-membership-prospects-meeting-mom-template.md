# Oniro membership prospects meeting <Year>wk<week number>

* Chair: <name>
* Scriba: <name>
* Schedule: Friday <day> <month> from 10:00 to 10:30 UTC


## Participants

(add/erase participants)

<name> <first letter of the surname>. <affiliation>, <name> <first letter of the surname>. <affiliation> and <name> <first letter of the surname>. <affiliation>. 

## Agenda

* <Topic1> - <Lead pname> <# of minutes> min
* <Topic2> - <Lead pname> <# of minutes> min
* <Topic3> - <Lead pname> <# of minutes> min
* AOB - <# of minutes> min

## MoM

### <Topic1>

Presentation

* <Description>
* <Description> #link <link>

Discussion

* <Description>
* <Description> #link <link>

### <Topic2>

Presentation

* <Description>
* <Description> #link <link>

Discussion

* <Description>
* <Description> #link <link>

### <Topic3>

Presentation

* <Description>
* <Description> #link <link>

Discussion

* <Description>
* <Description> #link <link>

### AOB

<Topic>

* <Description>
* <Description> #link <link>

Topics for next week

* <Topic> - <Lead name>
* <Topic> - <Lead name>


## Relevant links

* Repo: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management
* Actions workflow board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880
* Actions priority board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892 
* Other meetings: #link https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings 
