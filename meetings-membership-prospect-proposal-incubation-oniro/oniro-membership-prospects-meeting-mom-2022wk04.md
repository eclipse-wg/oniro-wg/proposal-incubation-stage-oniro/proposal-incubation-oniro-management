# Oniro membership prospects meeting 2022wk04

* Chair: Agustin
* Scriba: Agustin
* Schedule: Friday 2022-01-21 from 10:00 to 10:30 UTC


## Participants

(add/erase participants)

Sebastian S(Huawei).Andrea G. (Linaro), Agustin B. (EF), Gael B. (EF), Chiara DF (Huawei), Carlo P. (Array), Adrian O'S (Huawei), Davide R. (Huawei), Amit K. (Huawei). 

## Agenda

* Prospects follow up - Agustin 10 min
* Mobile World Congress - Agustin 10  min
* AOB - 10 min

## MoM

### Prospects follow up

Presentation

A couple of updates

Discussion

* Agreement to focus effort in getting the right contacts from the prosspect companies.
* #task set up the private repo and get the internal ok on wednesday.
* #task single process for pipeline management.
* #task import the info huawei already have into this single private.


### Mobile World Congress

Presentation



Discussion

* Consolidate the contact list this week.
* #task Meeting between Adrian and Agustin to work on the companies list.

### AOB ()



## Relevant links

* Repo: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management
* Actions workflow board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880
* Actions priority board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892 
* Other meetings: #link https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings 
