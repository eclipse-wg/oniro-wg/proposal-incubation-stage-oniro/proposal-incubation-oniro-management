# Oniro membership prospects meeting 2021wk50

* Chair: Agustin B.B.
* Scriba: Agustin B.B.
* Schedule: Friday 10th December from 10:00 to 10:30 UTC


## Participants

Participants: Davide R. (Huawei), Andrea G. (Linaro), Patrick O. (NOI Techpark), Agustin B.B. (EF), Carlo P. (Array), Chiara (Huawei), Gael B. (EF) and Adrian O'S. (Huawei)

## Agenda

* Value proposition and Membership ROI - 15 min
   * Andrea Gallo proposal. Discussion
* Initial list of organizations to contact - Agustin 5 min
* Meetings at MWC how-to - Agustin 5 min 
* AOB - 5 min

## MoM

### Value proposition and Membership ROI

Presentation

* Andrea: Proposal sent by mail to participants of this meeting. Gael answered.

Discussion

* Discussion about what is Silver about.
   * Gael and Agustin explain that the trigger for Silver level is the compatibility program. 
      * Compatibility program is the main value for being Silver.
   * Link to the video that explains the spec program from EF: #link https://drive.google.com/file/d/1N3Z53OOglstvfFNP-fRHMhEkL2NTgJbc/view?usp=sharing 
   * Link to the EF Spec process: #link https://www.eclipse.org/projects/efsp/  
   * Link to the Eclipse Foundation Specification Process Operations Guide: #link https://www.eclipse.org/projects/efsp/operations.php 
* #task Davide will introduce changes in the slides based on this conversation so we can go from a description of the slides to pitch decks.

### Initial list of organizations to contact 

Presentation

* Which organizations should be contacted at first? Input from Supporter organizations is valuable for the EF to contact them.
* Gael goes over the list we have created with organizations that are already Eclipse members. He suggests to start with a list of companies, contact names and introduction mails.

Discussion

* Davide provided names and status of prospects from Huawei side. 
   * The initial spreadsheet got extended
* Other organizations, please contact Agustin to provide yours so we do not collide.
* Shortlist those which we will talk to at MWC 2022

### Meetings at MWC how-to

Presentation

* Agustin proposes to manage the meetings and information related with them through a shared calendar.
* Information that we would need at this point:
   * How will we be introduced?
   * Which material should we prepare up front? Which should be sent in advance, if any?
   * Which days will the room be reserved? We would like to know before the Christmas break, if possible.
   * Schedule?
   * Location?

Discussion

No time to cover this point.

### AOB

Meeting to learn how the pipeline management has been done until now and how Eclipse does it.

* Sebastian S. from Huawei described to Gael and Agustin the template and process followed by Huawei to manage the business development activities. Gael explained to Sebastian other WG process.
   * The process is significantly heavier than the one followed by other Working Groups at Eclipse.
	* In general, the template looks good.	Agustin suggests to reduce the amount of information on the template as starting point. 

Next meeting

* Last meeting of the year. See you all on Jan. 14th 2022. Feliz Navidad.


## Relevant links

* Repo: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management
* Actions workflow board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880
* Actions priority board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892 
* Other meetings: #link https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings 
