# Oniro membership prospects meeting 2022wk06

* Chair: Agustin
* Scriba: Agustin
* Schedule: Friday 2022-02-11 from 10:00 to 10:30 UTC


## Participants

(add/erase participants)

Andrea G. (Linaro), Agustin B. (EF), Gael B. (EF), Chiara DF (Huawei), Carlo P. (Array), Adrian O'S (Huawei), Advide R. (Huawei). 

## Agenda

* MWC 2022 - Gael 15 min
* New prospects pipeline management proposal - Agustin 10 min
* AOB - 5 min

## MoM

### Mobile World Congress 2022

Presentation

* All the content for the presentations is done
* Went over the companies we have contacted and the ones left.

General call for action to contact people potentially interested in meeting/joining us at MWC 2022.

Discussion

* OSC will reduce the activities on the hotel room to 1 instead of 3.

### New prospects pipeline management proposal

Presentation

* Agustin showed the new proposal infographics.

   
Discussion

* General consensus on moving forward with the proposal
* Discussion about what to do with leads. Consensus on focussing for now in the prospects.
* #task Agustin to discuss with Clark the case of the leads in the area of marketing.

### AOB

* Reminder about the elections.

## Relevant links

* Repo: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management>
* Actions workflow board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880>
* Actions priority board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892> 
* Other meetings: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings> 
