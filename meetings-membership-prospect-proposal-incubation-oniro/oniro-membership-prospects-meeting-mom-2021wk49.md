# Oniro membership prospects meeting 2021wk49

* Chair: Agustin B.B.
* Scriba: Agustin B.B.
* Schedule: Friday 10th December from 10:00 to 10:30 UTC

Participants: Davide R. (Huawei), Andrea G. (Linaro), Patrick O. (NOI Techpark), Agustin B.B. (EF), Carlo P. (Array), Chiara (Huawei) and Adrian O'S. (Huawei)

## Agenda

* Value proposition and Membership ROI - Davide 15 min 
* Initial list of organizations to contact - Agustin 5 min
* Announcements and news - Agustin 5 min
* AOB 5 min

## MoM

### Value proposition and Membership ROI

Description

#link 
https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management/-/blob/main/value-proposition-goals-oniro/Oniro-Value-Prop-and-membership-roi.pptx 

Discussion

* Davide describes briefly what is his thinking behind each of the 5 concepts.
* Discussion about each of the concepts:
   * Openness
      * Agustin: what about vendor-neutral?
         * Carlo: he would like to see in the first box: open source, open governance and vendor-neutral
   * Since there is debate around the single word message we discuss that if we can approach it by defining three concepts first and then defining 
the label 
   * Patrick points out the cloud-free point so focus on the edge is not captured in these concepts but represents a core value.
* Next step: work on the mailing list the concepts and try to come next week with all input so it will be only about polishing the edges.
   
### Initial list of organizations to contact

No time to go over it. Point for next week.

### Announcements and news

* New page with the basic information to provide to any company that wants to 
join Oniro: #link https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Join%20Oniro 

### AOB 

* Adrian. We will be doing business development in March 2nd 2022 at the Mobile World Congress. There will be a room to have meetings. We need to prepare the meeting. Who will we meet there?
   * Telcos
   * SoC vendors
   * Who else?

## Relevant links

* Repo: #link 
https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management
* Actions workflow board: #link 
https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880
* Actions priority board: #link 
https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892 
* Other meetings: #link 
https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings
