# Oniro Supporter organizations meeting 2021wk49

Chair: Agustin B.B.
Scriba: Agustin B.B.
Schedule: Thursday 9th December from 16:00 to 16:45 UTC

Participants: Paul B. (EF), Davide R. (Huawei), Yves M. (EF), Sharon C. (EF) Andrea G. (Linaro), Patrick O. (NOI Techpark), Gael (EF), Gianluca V. (SECO), Andrea B. (Synesthesia), Andrea G. (Linaro, )Agustin B.B. (EF), Carlo P. (Array) and Adrian O'S. (Huawei)

## Agenda

* Value proposition update - Yves/Gianluca - 15 min
* Membership ROI - Davide 15 min 
* Announcements - Agustin 5min
* AOB 10 min


## MoM

### Value proposition update - Yves/Gianluca - 15 min

* No updates

### Membership ROI - Davide 15 min 

Description

* #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management/-/blob/main/value-proposition-goals-oniro/Oniro-Value-Prop-and-membership-roi.pptx

* Davide R. shows the preliminary slides. This deck includes the outcome of the 
Oniro Membership prospects meeting. It describes the value that Oniro delivers 
to Members and no members. 
* 5 core values are described: openness, diversity, compliance, robustness and 
longevity.
* Two target audience: CxO (growth) and R&D senior manager (savings).
* Tailored for device members. We should repeat the exercise for other 
profiles.

Discussion

* Patrick O. made the questions if for Academia can provide FTEs on top of the 
monetary investment.
* Carlo P. asked out loud what Oniro offers me to differentiate. The answer is 
that Oniro is not the place to differentiate but to work on the commodity.
* Paul B. suggest to includes the concept that orgs. will have to join EF first 
so it has to be considered.
* Agustin introduces the concept of trustable software.
* In tomorrow's meeting will will put effort in pushing the slide deck further.

### Announcements - Agustin 5min

* EF staff is on vacation from Dec. 23rd to Jan 3rd inclusive.

### AOB 10 min

Next meetings:
* Do we want to have the 23rd Dec session of this meeting?
   * We skip it.
* First meeting on 6th Jan or 13th Jan
   * Public holiday in some countries. Jan 13th is the next one.
* In tomorrow's meeting Gael suggest to add as an agenda point to put a list to 
organizations to contact.
   * Create a workflow.

## Relevant links

* Repo: #link 
https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/
proposal-incubation-oniro-management
* Actions workflow board: #link 
https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/
proposal-incubation-oniro-marketing/-/boards/880
* Actions priority board: #link 
https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/
proposal-incubation-oniro-marketing/-/boards/892 
* Other meetings: #link 
https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-
oniro/-/wikis/Meetings 

Other links, from chat
* #link 
https://blogs.windriver.com/wind_river_blog/2013/11/top-ten-reasons-you-can - 
trust-your-business-to-wind-river-linux-part-3/
* #link https://www.trustablesoftware.com/
