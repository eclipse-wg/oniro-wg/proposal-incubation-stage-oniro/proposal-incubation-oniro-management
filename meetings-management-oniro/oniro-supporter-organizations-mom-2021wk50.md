# Oniro Supporter organizations meeting 2021wk50

* Chair: Agustin
* Scriba: Agustin
* Schedule: Thursday 15th December from 16:00 to 16:45 UTC

## Participants

Paul B. (EF), Sharon C. (EF) Andrea G. (Linaro), Patrick O. (NOI Techpark), Gael (EF), Andrea B. (Synesthesia), Agustin B.B. (EF), Carlo P. (Array) and Adrian O'S(Huawei) 

## Agenda

* Value Proposition: update - Yves/Gianluca 5 min
* Comittees representatives - Agustin 5 min
* Discussions about specifications - Davide R. 10 min
* Projects proposal: update - Agustin 5 min
* AOB - 20 min

## MoM

### Value Proposition: update

Presentation

* No updates

### Committees representatives

Presentation

* We need all Supporter organizations to think about who should be the representative and the proxy at each committee (when appropiate):
   * Steering C.
   * Marketing C.
   * Specicifcations C.
* We invite the organization to discuss who should be the chair of each of those committees.

### Discussions about specifications

Presentation: agustin

* Davide R. proposed to start with the meeting series about specifications to prepare the initial meeting and learn about it.
* We will need to clarify who should participate. Which profile is needed.

Discussion
 
* We need to learn about this committee. Bringing somebody that tell us about how these committees work is useful. #task Agustin
* Paul Buck chairs one specification committee.
   * Their main responsibility is directly related with the Spec process at Eclipse Foundation. 
   * Wayne can provide details on how this process is implemented.
   * Paul Buck provides a summary on the workflow of a spec.
   * Separation of concerns between the spec project and the committee. 
		* Content -> project
		* Process -> committee
		* Compatibility program -> committee 
* Every spec has a project page.
* Communication protocols and APIs should be a matter of specs.
* Interoperability will be a subject of these specs, having Oniro as open source implementation (compatible implementation).
* Compatible implementation is the EF terminology.
* Andrea G. explains how at Linaro, when it comes to specifications, the community side and the members side are governed differently. He describes an example. There are fundamental similarities with how Eclipse manage them.

Relevant links to read (what a nice read for Christmas!):

* Eclipse Foundation Specification Process #link https://www.eclipse.org/projects/efsp/
* Eclipse Foundation Specification Process Operations Guide #link https://www.eclipse.org/projects/efsp/operations.php


### Projects proposal: update

Presentation

* First bulk of project proposals submitted by Amit
* First review done by EF. Questions and requests sent to submitter.
* Changes based on those questions and requests are being developed by Amit and collaborators (submitter).
* Next week there is a meeting between EF and submitter scheduled to sort out the missing points, if anything.
* Once EF feels comfortable with the project proposals, in agreement with the corresponding process, the proposals will be published for community review.
* Once this step of the process is over, and understanding the dependency with the tools migration plan which is WIP, the initial contribution will be submitted to Eclipse Foundation.

Discussion

* Call for support to Amit. The projects proposal and code migration are high priority topic.
* At some point early next month it would be good to have a summary of this task sent to the mailing list and being able to ask questions about it to the involved people. #task agustin

### AOB

Reminders
* Next meeting will take place on the second week of January 2022

Thank you from Agustin to all participants for the warm welcome and the onboarding efforts done this year. Feliz Navidad.

## Relevant links

* Repo: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management
* Actions workflow board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880
* Actions priority board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892 
* Other meetings: #link https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings 
