# Oniro Supporter organizations meeting 2022wk02

* Chair: Agustin
* Scriba: Agustin
* Schedule: Thursday 2022-01-13 from 16:00 to 16:45 UTC

## Participants

Chiara (Huawei) Sharon C. (EF) Andrea G. (Linaro),Patrick O. (NOI Techpark), Gael (EF), Andrea B. (Synesthesia), Agustin B.B. (EF), Carlo P. (Array) and Adrian O'S(Huawei) Paul B. (Huawei), Philippe Coval (Huawei)

## Agenda

* News about the Oniro Steering Committee kick-off - Agustin 5 min
* Oniro-core projects proposal. Next steps - Agustin 5 min
* AOB - 15 min

## MoM

### News about the Oniro Steering Committee kick-off

Presentation

* Kick-off date will be January 27th
* Participation agreements need to be signed asap
* In the next meeting Agustin will advance some details about the meeting format. 

Discussion

* Paul B. suggest that if we cannot move the day for the SC meetings, we at least can move it to 15 hours CET?

### Oniro-core projects proposal. Next steps

Presentation

* Project proposal approved: #link https://projects.eclipse.org/proposals/eclipse-oniro-core-platform
* oniro Core Platform project page: #link https://projects.eclipse.org/projects/oniro.onirocore 
* You can follow the process of the oniro-core creation in this ticket: https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/issues/168#project-resources-to-be-created
* #task Every contributor to booting.oniroproject.org should have an EF account before the code is migrated. Unexpected issues, most with low impact, might happen if this is not the case.

* Next steps:
   * Provide the missing information in the project page that can be added now.
   * #task Every contributor to booting.oniroproject.org should have an EF account before the code is migrated. Unexpected issues, most with low impact, might happen if this is not the case.
   * The next step after provisioning is to send the project code over to the IP Team for their review as an initial contribution.

Discussion



### AOB

* Next week the Value proposition draft will be presented.
* Information you might want to check:
   * Join us wiki page: #link  https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Join_Oniro
   * Oniro stand at FOSDEM: #link  https://stands.fosdem.org/stands/oniro_project/
   * Eclipse stand at FOSDEM (we need to add information about Oniro): https://stands.fosdem.org/stands/eclipse-foundation/


## Relevant links

* Repo: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management
* Actions workflow board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880
* Actions priority board: #link https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892 
* Other meetings: #link https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings 
