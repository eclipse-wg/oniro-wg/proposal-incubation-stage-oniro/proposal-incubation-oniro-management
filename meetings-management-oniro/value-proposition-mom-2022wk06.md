# Oniro Value proposition 2022wk06

* Chair: Agustin
* Scriba: Agustin
* Schedule: Thursday 2022-02-10 from 16:00 to 16:45 UTC

## Participants

Aurore (Huawei) Patrick O. (NOI Techpark), Gael (EF), Agustin B.B. (EF), Yves M (huawei), Andrea B. (Synesthesia), Carlo P. (Array), Davide R. (Huawei), Gianluca V. (Seco), Philippe C. (Huawei), Chiara DF (Huawei), Donghi (Huawei), Andrea G. (Linaro), Sebastian S.(Huawei) and Adrian O'S(Huawei)

## Agenda

* Value proposition - Gael 40 min
* AOB - 5 min

## MoM

### Value proposition

Presentation

* Gael goes over the deck
* Aurore explains the process that led to the slide deck
* #task Aurore to share the document and slides so every body works on it.

Discussion

* Oniro has a lower learning curve than other OS in this field because standard software is used.
* Questions about personas.
* Pay attention to the fact that fragmentation cannot be fought with yet another option.
* Idea of one OS for big and small devices. Adrian has a slide. That message has been provided before. We need to attach to it why this time is different.
* Start-up ecosystems might benefit from something like Oniro
* Agreement in recognising Aurore and Gael's effort.

### AOB

* Philippe comments he is planning to present Oniro in an internal Huawei technical event.
* Some of the representatives will get together in Milan on Feb 23rd.

## Relevant links

* Repo: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management>
* Actions workflow board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880>
* Actions priority board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892> 
* Other meetings: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings> 
