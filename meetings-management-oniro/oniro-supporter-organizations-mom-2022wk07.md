# Oniro Supporter organizations meeting 2022wk07

* Chair: Agustin
* Scriba: Agustin
* Schedule: Thursday 2022-02-17 from 16:00 to 16:45 UTC

## Participants

Sharon C. (EF), Gael (EF), Agustin B.B. (EF), Paul B. (EF), Chiara DF (Huawei), Aurore (Huawei), Patrick O. (Huawei), Bill F. (Linaro) and Carlo P. (Array)

## Agenda

* News - Agustin 5 min
* Program Plan and Budget - Paul B. and Sharon C. 30 min
* Value proposition: next steps - Gael 5 min
* AOB - 5 min

## MoM

### Value Proposition

Presentation

Aurore, Yves and Gael will meet in Paris. Then they will fly to Milan to meet Davide and other Oniro Members representatives.

Discussion

* Aurora request to read the document she sent and provide feedback.
* A video will be created as intro for our talks. S Something inspirational.


### News

Presentation

* Elections finished. 
   * Oniro WG SC: Carlo P. as Silver representative and Amit K. as Committers representative elected by acclamation (only candidates).
      * First meeting is being scheduled
   * Oniro WG Marketing Committee
      * First meeting on March 9th
* New mailings list coming. Be ready for an announcement at the oniro-wg and oniro-dev on the public ones. Oniro WG SC and Marketing Committee Members will be added automatically. You will receive a notification. 

Discussion


### Program Plan and Budget

Presentation

* Paul Buck presents a slide deck targetting the EF Working Groups.


Discussion



* Questions about the timeline to create and approve the Program Plan and Budget in Oniro's case.
* Paul Buck details the budget creation and approval workflow.
* What happens is the budget substantially changes during the year?
   * Budget should change and be approved.

### AOB

* Oniro Presentation at MWC 2022 Tuesday 1st March at 9:00 
   * You can join online. Please check the Oniro calendar.
* In the next few days, since we will be rolling the Oniro WG Steering Committee, the Marketing Committee, the new prospect pipeline management process, the new mailing list... there will be activity on the mailing list and the calendar to accommodate all this.
   * Please check the Oniro Meetings page to stay up to day if you do not follow closely the oniro-wg mailing list.

## Relevant links

* Repo: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management>
* Actions workflow board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880>
* Actions priority board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892> 
* Other meetings: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings> 
