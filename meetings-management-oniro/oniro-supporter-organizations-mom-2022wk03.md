# Oniro Supporter organizations meeting 2022wk03

* Chair: Agustin
* Scriba: Agustin
* Schedule: Thursday 2022-01-20 from 16:00 to 16:45 UTC

## Participants

Aurore (Huawei) Sharon C. (EF),Patrick O. (NOI Techpark), Gael (EF), Agustin B.B. (EF), Carlo P. (Array), Davide R. (Huawei), Gianluca (Seco) and Adrian O'S(Huawei)

## Agenda

* Value proposition - Gael 15 min
* Oniro WG Steering Committee kick-off. Next steps - Agustin 25 min
* AOB - 5 min

## MoM

### Value proposition

Presentation

* The goal is to describe the proposal. The we will have a Q&A.
* The discussion about the topic will take place in a dedicated meeting in two weeks.
   * Next week we will have the Oniro SC kick-off
* Proposal (draft) presented by Gael 
      * #task Gael will share the value deck on Wednesday 26th
* Walk-through the main arguments across the matrix.


Discussion

* We are getting closer to convergence between the work done by different groups within Oniro.
* The document is a matrix between personas, arguments and consumer profile.
* Next proposed step, if people agree on the content in the matrix is to create slides that we can use to compose different slide decks
* Adrian suggest to have the decks finished by MWC. #agreed
* The content presented by Gael is well received.

### Oniro WG Steering Committee kick-off. Next steps

Presentation

* Description of the next steps.
* Proposed changes in the Charter.
   * #link <https://www.eclipse.org/lists/oniro-wg/msg00094.html>
* EF WG Process #link <https://www.eclipse.org/org/workinggroups/process.php#wg-proposal>


Discussion

Marketing Committee elections will take place as well.

#### Summary

Chronology

Before the meeting

* Eclipse Foundation has a notification from each organization of the name/mail of the company representative at the SC as well as the alternate.
* The Oniro WG SC kick off will take place on January 27th from 16:00 to 17:00 hours
   * This is the current schedule of this meeting.
   * The calendar invitation will be changed to reflect the new meeting information and invitees. 
   * Only representatives of companies who have signed the Participation agreement will be invited.
   * Alternates will participate at discretion of the Founding Member company at this first meeting. Then it is up to the SC to decide..
   * The agenda will be included in the calendar invitation.

About the meeting

* Agustin will be the chair of this first session. The minutes taker (scribe) will be Agustin/Sharon. 
* The meeting will start with the approval of the Charter by the Strategic Members (in our case, Huawei).
   * Reminder of the changes introduced in the Charter and communicated.
* Sharon C. and Paul Buck will go over the Oniro WG SC launch deck which includes the most relevant information that the representatives of the Oniro Members should review and understand at this point, as well as the next steps. 
* An interim chair will be appointed/elected until the SC is fully functional and elect one for a year period (until the next election period).
* Election Announcement  will be called so the election process begins. Such process will be driven by the Eclipse Foundation. 

Considerations

* WG SC meetings are usually bi-weekly or monthly. We will define the cadence and the schedule once the elections have finished, during the first SC meeting.
* The attendance of the alternates is up to the Member representative.

To Dos

* #task Sign the participation agreement if you haven’t done so and submit it.
* #task Please send to Agustin the name and mail of the company representative and his/her alternate this week in order to invite them to next week’s meeting. 
You can also add it in this ticket: <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management/-/issues/9> 


### AOB

* Track the Provisioning stage of the oniro core project: #link <https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/issues/168> 
* Track the work done for FOSDEM: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Events/FOSDEM> 
* Link to the oniro-dev mailing list: 


## Relevant links

* Repo: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-management>
* Actions workflow board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880>
* Actions priority board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892> 
* Other meetings: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings> 
