# Oniro Supporter organizations meeting 2021wk47

Schedule: Thursday 25th November from 16:00 to 16:45 UTC

Participants: Carlo P. (Array), Paul B. (EF), Sharon C. (EF), Wayne B. (EF), 
Davide R. (Huawei), Andrea B. (Synesthesia), Andrea G. (Linaro), Patrick O. 
(NOI Techpark), Yves M. (EF) and Agustin B.B. (EF). 

## Agenda

* Next steps to move the Oniro WG from Proposal to Incubation to Operational + 
Q&A - Paul Buck  20 min
* Reminders - Agustin 5 min
* AOB 20 min


## MoM

### Next steps to move the Oniro WG from Proposal to Incubation to Operational

* Paul Buck, from EF, goes over the process to go from the Proposal stage where 
we are to Incubation and Operational.
* Andrea Basso request the Participation Agreement. 
   * Access the Participation Agreement from the WG Explore pages: 
https://www.eclipse.org/org/workinggroups/explore.php
   * Participation Agreement: 
https://www.eclipse.org/org/workinggroups/wgpa/oniro-working-group-participation
-agreement.pdf 
* #task Agustin to share Paul Buck's slide deck among participants.
* Discussion opened by Andrea Gallo about the need or convenience to review the 
Charter before the SC is formed and the consequences of that process. The main 
drivers for opening at this point this topic are:
   * Differentiation between Gold and Silver can be improved.
   * There was no discussion about this specific point during the review step.
   * It can be that the SC does not have the require number of members.
   * Paul Buck makes the group aware that this change is expected after the SC 
is formed. If it is at this point, we will not hit the expected deadline.
   * The group is aware of Jan 20th as deadline for forming the Oniro SC.
   * Paul reinforces the role of EF to support the organizations in creating 
the SC so the WG can move into operational quickly.


### Reminders

* Schedule and ground rules for the Oniro Membership prospects 
meeting series. The poll is out. Please fill it in so Agustin can schedule the 
meeting series next Monday.
* A new Oniro Calendar has been created to reduce the number of notifications 
the invitees need to manage. The links to this calendar has been sent to the 
oniro-wg mailing list.
* Links to the Oniro WG gitlab Group will be sent this week.
   * The Membership prospects documentation will need to be open, as everything 
we need to do at this stage of the workflow.

### AOB

* Carlo Piana raise the concern of managing the Membership prospects using 
files that are open.
   * Agustin shares the concern but points out that the principles here and the 
fact that Organizations has not signed yet the participation agreement makes 
impossible to provide private instances within the EF to hold information that 
can be subject to regulations. We will need to deal with this topic this way 
until the SC is formed.
