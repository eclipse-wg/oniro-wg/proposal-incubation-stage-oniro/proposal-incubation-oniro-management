# Oniro Supporter organizations meeting 2021wk48

Chair: Agustin B.B.
Scriba: Agustin B.B.
Schedule: Thursday 1st December from 16:00 to 16:45 UTC

Participants: Wayne B. (EF), Davide R. (Huawei), Yves M. (EF), Andrea G. 
(Linaro), Patrick O. (NOI Techpark), Gael (EF), Agustin B.B. (EF) and Adrian 
O'S. (Huawei)


## Agenda

* Charter review. from the EF PoV - Gael B. - 10 min
* Value proposition updates - Yves/Gianluca - 5 min
* Project proposals - Adrian 10 min 
* Announcements - Agustin 10min
   * Oniro Membership prospects meeting series
   * Gitlab walk-through
   * Participation Agreement
* AOB 10 min


## MoM

### Charter review. from the EF PoV

Gael presents the proposal to keep the charter as it is for 2022 and change it 
during 2022 to be effective in 2023.

Meetings being set between Gael and future Founder Members to go over the 
participation agreement.

Discussion

Davide agrees with the approach. Work on validating the current design of 
membership levels and fees is being done.
   * Early work suggest the charter could stay as they are.
   * The work is focused at the moment in the Value proposition vs the value 
provided to the members proposed in the Charter.

Guest Members at EF WG are usually research entities, non-profit and academia, 
Universities. It is not designed as entry level for other kind of 
organizations. Entry level is Silver.


### Value proposition updates 

The topics is transferred to this group. 

No content updates.

* Yves: The value proposition discussion is about why consuming Oniro?
* Davide: The effort lead by Davide answers the question why joining Oniro WG. 
If there are overlaps between the two efforts, we will manage it. They seem 
complementary. 

### Project proposals 

Where are we?

Adrian explains that the proposal is in active development. Those working on it 
expect EF review before submitting it, as agreed during the Oniro f2f meeting 
in Solda.

Reviewers from EF have access. It is the first time for those behind the 
proposals so guidance from EF welcome.

Two projects are expected:
1. Bulk of the OS
2. Blueprints have been separated.

Later on maybe AI and others.

The one being reviewed is the OS/Platform. One (big) proposal for now and maybe 
in the future we split them.
   * Wayne confirms this is a natural approach.

Wayne will review the proposal for the first time asap.

Repo #link 
https://git.ostc-eu.org/davidinux/ohos-working-group/-/merge_requests/32 


### Announcements 

* Oniro Membership prospects meeting series.
* Gitlab walk-through. Some structured #link 
https://www.eclipse.org/lists/oniro-wg/msg00028.html 
* Participation Agreement. Covered already by Gael in the first point.

### AOB

